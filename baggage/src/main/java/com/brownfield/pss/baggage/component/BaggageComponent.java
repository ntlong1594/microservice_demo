package com.brownfield.pss.baggage.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brownfield.pss.baggage.entity.Baggage;
import com.brownfield.pss.baggage.repository.BaggageRepository;

@Component 
public class BaggageComponent {
	private static final Logger logger = LoggerFactory.getLogger(BaggageComponent.class);

	BaggageRepository repository;
	Sender sender;
	
	public BaggageComponent(){
		
	}
	@Autowired
	public BaggageComponent(BaggageRepository repository,Sender sender){
		this.repository = repository;
		this.sender = sender;
	}

	public Baggage getBaggage(String checkInId){ 
		logger.info("Looking for fares checkInId "+ checkInId);
		return repository.getBaggageByCheckInID(checkInId);
	}

	public long createDefaultBaggage(Baggage baggage) {
		logger.info("Create Default Baggage  ");
		repository.save(baggage);
		logger.info("Save Default Baggage to DB ");
		sender.send(baggage.getId());
		return baggage.getId();
	}
}
