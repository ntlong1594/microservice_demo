package com.brownfield.pss.baggage.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@EnableBinding(BaggageSource.class)
@Component
public class Sender {

	public Sender() {

	}

	/**
	 * RabbitMessagingTemplate template;
	 * 
	 * @Autowired Sender(RabbitMessagingTemplate template){ this.template =
	 *            template; }
	 * @Bean Queue queue() { return new Queue("CheckInQ", false); }
	 * 
	 *       public void send(Object message){ template.convertAndSend("CheckInQ",
	 *       message); }
	 **/

	@Output(BaggageSource.CHECKINQ)
	@Autowired
	private MessageChannel messageChannel;

	public void send(Object message) {
		// template.convertAndSend("CheckInQ", message);
		System.out.println("Output CheckInQ");
		messageChannel.send(MessageBuilder.withPayload(message).build());
	}
}

interface BaggageSource {
	public static String CHECKINQ = "checkInQ";

	@Output(BaggageSource.CHECKINQ)
	public MessageChannel checkInQ();

}