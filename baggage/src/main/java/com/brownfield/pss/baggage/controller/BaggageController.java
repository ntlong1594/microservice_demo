package com.brownfield.pss.baggage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brownfield.pss.baggage.component.BaggageComponent;
import com.brownfield.pss.baggage.entity.Baggage;

@RestController
@CrossOrigin
@RequestMapping("/baggage")
public class BaggageController {
	private static final Logger LOG = LoggerFactory.getLogger(BaggageController.class);

	BaggageComponent component;

	@Autowired
	BaggageController(BaggageComponent component) {
		this.component = component;
	}

	@RequestMapping(value = "/getBaggage", method = RequestMethod.GET)
	Baggage getBaggage(@RequestParam(value = "checkInId") String checkInId) {
		LOG.info("Get Baggage of checkInId : " + checkInId);
		return component.getBaggage(checkInId);
	}

	@RequestMapping(value = "/createBaggage", method = RequestMethod.POST)
	public long createDefaultBaggage(@RequestBody Baggage baggage) {
		LOG.info("Create Baggage  : " + baggage.getId());
		return component.createDefaultBaggage(baggage);
	}
}
