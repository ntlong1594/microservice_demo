package com.brownfield.pss.client;

public class Baggage {

	long id;

	private int baggageWeight;
	private double baggagePrice;
	long checkInID;

	public Baggage() {
		super();
	}

	
	public Baggage(long id, int baggageWeight, double baggagePrice, long checkInID) {
		super();
		this.id = id;
		this.baggageWeight = baggageWeight;
		this.baggagePrice = baggagePrice;
		this.checkInID = checkInID;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(int baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	public double getBaggagePrice() {
		return baggagePrice;
	}

	public void setBaggagePrice(double baggagePrice) {
		this.baggagePrice = baggagePrice;
	}

	public long getCheckInID() {
		return checkInID;
	}

	public void setCheckInID(long checkInID) {
		this.checkInID = checkInID;
	}

	@Override
	public String toString() {
		return "Baggage [id=" + id + ", baggageWeight=" + baggageWeight + ", baggagePrice=" + baggagePrice
				+ ", checkInID=" + checkInID + "]";
	}


	
}
